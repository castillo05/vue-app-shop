
import Login from './components/Login.vue';
import Products from './components/Products.vue';
import Home from './components/Home.vue';
import NewProduct from './components/NewProduct.vue';
import ProductDetail from './components/Product-Detail.vue';
   
 const  routes=[
        {
            path:'/',
            name:'home',
            component:Home
        },
        {
            path:'/login',
            name:'login',
            component:Login
        },
        {
            path:'/products',
            name:'products',
            component:Products
        },
        {
            path:'/new-product',
            name:'new-product',
            component:NewProduct
        },
        {
            path:'/product-detail/:id',
            name:'product-detail',
            component:ProductDetail
        }
    ]

export default routes;
