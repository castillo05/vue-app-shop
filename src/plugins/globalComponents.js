import Badge from "../components/components/Badge"
import BaseAlert from "../components/components/BaseAlert";
import BaseButton from "../components/components/BaseButton";
import BaseInput from "../components/components/BaseInput";
import BasePagination from "../components/components/BasePagination";
import BaseProgress from "../components/components/BaseProgress";
import BaseRadio from "../components/components/BaseRadio";
import BaseSlider from "../components/components/BaseSlider";
import BaseSwitch from "../components/components/BaseSwitch";
import Card from "../components/components/Card";
import Icon from "../components/components/Icon";

export default {
  install(Vue) {
    Vue.component(Badge.name, Badge);
    Vue.component(BaseAlert.name, BaseAlert);
    Vue.component(BaseButton.name, BaseButton);
    Vue.component(BaseInput.name, BaseInput);
    Vue.component(BasePagination.name, BasePagination);
    Vue.component(BaseProgress.name, BaseProgress);
    Vue.component(BaseRadio.name, BaseRadio);
    Vue.component(BaseSlider.name, BaseSlider);
    Vue.component(BaseSwitch.name, BaseSwitch);
    Vue.component(Card.name, Card);
    Vue.component(Icon.name, Icon);
  }
};
