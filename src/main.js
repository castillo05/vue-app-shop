import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './router';

import Argon from "./plugins/argon-kit";

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// Vue router
Vue.use(VueRouter)

// Argon
Vue.use(Argon);

Vue.config.productionTip = false

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
 
}).$mount('#app')
